from functools import wraps

from flask import g
from flask import url_for, render_template, request, redirect, session

from app import app, db
from app.models.user import User


@app.before_request
def load_user():
    """Store logged user"""
    try:
        username = session['username']
        user = User.query.filter_by(username=username).first()
    except KeyError:
        user = None
    g.user = user


def login_required(view_func):
    """Login Decorator"""
    @wraps(view_func)
    def wrap(*args, **kwargs):
        if not session.get('logged_in'):
            return redirect(url_for('login'))
        else:
            return view_func(*args, **kwargs)
    return wrap


@app.route('/login', methods=['GET', 'POST'])
def login():
    """Login Form"""
    if request.method == 'GET':
        return render_template('login.html')
    else:
        name = request.form['username']
        passw = request.form['password']
        try:
            data = User.query.filter_by(username=name, password=passw).first()
            if data is not None:
                session['logged_in'] = True
                session['username'] = name
                return redirect(url_for('index'))
            else:
                return 'Don\'t Login'
        except:
            return 'Don\'t Login Exc'


@app.route('/register/', methods=['GET', 'POST'])
def register():
    """Register Form"""
    if request.method == 'POST':
        new_user = User(username=request.form['username'], password=request.form['password'])
        db.session.add(new_user)
        db.session.commit()
        return render_template('login.html')
    return render_template('register.html')


@app.route('/logout')
def logout():
    """Logout Form"""
    session['logged_in'] = False
    return redirect(url_for('index'))
