from flask_wtf import Form
from wtforms import TextField, TextAreaField
from wtforms.validators import Required


class EntityForm(Form):
    key = TextField('key', validators=[Required()])
    value = TextAreaField('value', validators=[Required()])
