# Simple Access App

Test task requirements:

Part 1 - Backend: write a Python Flask microservice that has two API methods: “get” and “set”. The method “get” takes a “key” argument and returns the current value associated with the key, if it exists. The method “set” takes two arguments, a “key” and a “value”, and sets the value of “key” to the specified value. The data should be persisted to a database using sqlalchemy.


Part 2 - Frontend: design and code a front end for the above key/value API service. Users should be able to load an HTML page and get/set keys. Use HTML, CSS, and Javascript/JQuery as necessary.


Part 3 - Advanced: add a user model to the above service. A user should be able to sign up, log in, and log out. While logged in, they should only be able to interact with and modify keys they have created - do not allow users to see or modify another users key/value pairs. Logged out users should not be able to do anything other than log in.

## Requirements.txt
    Flask
    flask_wtf
    Flask-SQLAlchemy
    psycopg2

## Setup Repo
```
virtualenv -p python3 /.
source bin/activate
git clone https://gitlab.com/ntodosiychuk/SimpleAccessApp.git
cd SimpleAccessApp
pip install -r requirements.txt
python run.py
```