from flask import g
from flask import render_template

from app import app
from app.forms.entity import EntityForm
from app.models.entity import Entity
from app.views.auth import login_required


@app.route('/', methods=['GET'])
@login_required
def index():
    """Index view"""
    form = EntityForm()
    return render_template('index.html', form=form,
        entities=Entity.query.filter_by(user_id=g.user.id).all())
