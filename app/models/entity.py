from .. import db


class Entity(db.Model):
    """Simple Entity model for storing key/value"""
    id = db.Column(db.BIGINT, primary_key=True, autoincrement=True)
    key = db.Column(db.String(100), unique=True)
    value = db.Column(db.String(100))

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    def __repr__(self):
        return '<Entity - {}>'.format(self.key)
