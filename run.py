#!/usr/bin/env python

from app import app, db


if __name__ == '__main__':
    app.debug = True
    db.create_all()
    # TEST secret :)
    app.secret_key = '123'
    app.run(host='0.0.0.0', debug=True, port=5000)
