from flask import g, jsonify, request

from app import app, db
from app.models.entity import Entity
from app.views.auth import login_required


@app.route('/api/v1/entity', methods=['GET', 'POST'])
@login_required
def api_get_entity():
    """Return Entity specified by key"""
    data = {}
    key = request.form.get('key')
    e = Entity.query.filter_by(key=key).first()
    if e:
        data = {
            'key': e.key,
            'value': e.value,
        }
    return api_response(status='success', data=data)


@app.route('/api/v1/entity/create', methods=['POST'])
@login_required
def api_create_entity():
    """Create new Entity"""
    if request.method == 'POST':
        entity = Entity(key=request.form.get('key'),
                        value=request.form.get('value'),
                        user_id=g.user.id)
        db.session.add(entity)
        db.session.commit()
        return api_response(status='success')


def api_response(status='error', data=None, message=None):
    return jsonify({'status': status, 'data': data, 'message': message})
