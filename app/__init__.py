"""Flask Login Example and instagram fallowing find"""
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
# TODO: move to local_config.py
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://federer:grandestslam@localhost:5432/test'
db = SQLAlchemy(app)

from .views import auth, index, api
